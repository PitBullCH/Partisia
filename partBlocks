#!/bin/bash

#
# validate argument #1 as time (minutes)
#
INPUT_MINS=${1}
# if ! [ "${INPUT_MINS}" -eq "${INPUT_MINS}" ] 2> /dev/null
if ! [ "${INPUT_MINS}" -eq "${INPUT_MINS}" 2> /dev/null ]
then
    printf "\nERROR >>> Need integer value in minutes\n\n"
    exit 1
fi

#
# validate argument #2 as node index (note: cannot currently programmatically check this is a valid node index)
#
INPUT_NODE=${2}
if [ "${INPUT_NODE}" ] && ! [ "${INPUT_NODE}" -eq "${INPUT_NODE}" ] 2> /dev/null
then
    printf "\nERROR >>> Need integer value node index\n\n"
    exit 2
fi

#
# reject any further arguments
#
INPUT_WTF=${3}
if [ "${INPUT_WTF}" ]
then
    printf "\nERROR >>> Enough ! I've had it with all the arguments !! NO MORE ARGUMENTS !!!\n\n"
    exit 3
fi

#
# use "docker ps" to get the correct docker ID (there may be more than one Docker running !)
# use "docker inspect" to get IP properly, don't dick around with painful manipulation of "ps" output
# use "docker ps" to get the port - initially assume it's 8080, switch to 80 if it's not
#
DOCKER_ID=`docker ps | grep pbc.*reader  | cut -d" " -f1`
if [ ! ${DOCKER_ID} ]; then
    printf "\nERROR >>> Partisia node or possibly Docker is not running"
    exit 1
else
    DOCKER_IP=`docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' ${DOCKER_ID}`
    if [ ! ${DOCKER_IP} ]; then
        printf "\nERROR >>> Partisia node or possibly Docker is not running"
        exit 2
    else
        DOCKER_PORT=8080
        [ -z "`docker container ls | grep ${DOCKER_ID} | grep ${DOCKER_PORT}`" ] && DOCKER_PORT=80
    fi
fi

#
# announce ourself
#
printf "\nINFO >>> Checking back over %d minutes" ${INPUT_MINS}
[ ! -z "${INPUT_NODE}" ] && printf "\nINFO >>> Checking blocks for node %d" ${INPUT_NODE}
printf "\nINFO >>> Docker is using %s" ${DOCKER_IP}

#
# for each of Shard Main / 0 / 1 use local curl to obtaint the stats, decide if specific node output is needed also (there might be nothing !)
#
# Shard Main
#
printf "\n\nINFO >>> Block Stats for Shard Main over last %d minutes\n    " ${INPUT_MINS}
BLOCK_SM=`curl -s -X GET http://${DOCKER_IP}:8080/metrics/blocks/${INPUT_MINS}`
printf "%s" ${BLOCK_SM}
[ ${INPUT_NODE} ] && printf "\n     >>> Node %d = %d blocks" ${INPUT_NODE} `echo ${BLOCK_SM} | sed -e 's/[{}]/''/g' | awk -v RS=',' -F: '{print $1 ":" $2}' | grep \"${INPUT_NODE}\":  | cut -d":" -f2`
#
# Shard0
#
printf "\n\nINFO >>> Block Stats for Shard0 over last %d minutes\n    " ${INPUT_MINS}
BLOCK_S0=`curl -s -X GET http://${DOCKER_IP}:8080/shards/Shard0/metrics/blocks/${INPUT_MINS}`
printf "%s" ${BLOCK_S0}
[ ${INPUT_NODE} ] && printf "\n     >>> Node %d = %d blocks" ${INPUT_NODE} `echo ${BLOCK_S0} | sed -e 's/[{}]/''/g' | awk -v RS=',' -F: '{print $1 ":" $2}' | grep \"${INPUT_NODE}\":  | cut -d":" -f2`
#
# Shard1
printf "\n\nINFO >>> Block Stats for Shard1 over last %d minutes\n    " ${INPUT_MINS}
BLOCK_S1=`curl -s -X GET http://${DOCKER_IP}:8080/shards/Shard1/metrics/blocks/${INPUT_MINS}`
printf "%s" ${BLOCK_S1}
[ ${INPUT_NODE} ] && printf "\n     >>> Node %d = %d blocks" ${INPUT_NODE} `echo ${BLOCK_S1} | sed -e 's/[{}]/''/g' | awk -v RS=',' -F: '{print $1 ":" $2}' | grep \"${INPUT_NODE}\":  | cut -d":" -f2`

printf "\n\n"
