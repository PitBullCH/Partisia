# Partisia

A bunch of stuff that may or may not be useful for the Partisia Blockchain (MPC) project and the node operator community.

## partBlocks
Displays the numbers of blocks going back the last X minutes and which nodes produced them, optionally also supply your node index Y to see how many blocks it produced.

## partID
Displays the chain ID (essentially the release version of the blockchain software).

## partRT
Displays run time information relating to memory usage and available processors.
